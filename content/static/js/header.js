$(document).ready(function() {
    let $title = $('#title'),
        $navbar = $('#navbar-inner'),
        h1Height = $('h1').height()

    if ($title.length) {

        // set sticky from JS for graceful degradation if JS is not available
        $title.css('position', 'sticky')

        let startY = $navbar.height()

        let finalHeight = h1Height * 0.7 + 20;
        $title.css('top', finalHeight - $title.outerHeight() + 'px')

        let goalY = startY + $title.height() - finalHeight
        if ($title.hasClass('with-image')) {
            goalY += 120;
        }

        console.log('startY ' + startY);
        console.log('goalY ' + goalY);
        console.log('title height: ' + $title.outerHeight());

        $(window).on('scroll', function() {
            let scrollPosition = $(window).scrollTop()

            let relativeScroll = (scrollPosition - startY) / (goalY - startY)
            if (relativeScroll < 0) {
                relativeScroll = 0
            } else if (relativeScroll > 1) {
                relativeScroll = 1
            }

            let opacity = 1 - relativeScroll;
            $('#title-image').css('opacity', opacity)
            let scale = 1 - 0.3 * (1 - opacity)
            $('h1').css('transform', 'scale(' + scale + ')')
            let offset = 0.9 * h1Height * (1 - scale)
            $('h1').css('top', offset + 'px')
        })
    }
})

