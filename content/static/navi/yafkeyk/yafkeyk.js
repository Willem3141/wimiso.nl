var weatherStrings = {
    // thunderstorm
    "200": ["lu rawmpxom, ulte tompameyp zup", "weather-storm",                 "weather-storm",                   "dark"],
    "201": ["lu rawmpxom, ulte tompa zup",     "weather-storm",                 "weather-storm",                   "dark"],
    "202": ["lu rawmpxom, ulte tompa 'eko",    "weather-storm",                 "weather-storm",                   "dark"],
    "210": ["lu rawmpxomtsyìp",                "weather-storm",                 "weather-storm",                   "dark"],
    "211": ["lu rawmpxom",                     "weather-storm",                 "weather-storm",                   "dark"],
    "212": ["lu apxa rawmpxom",                "weather-storm",                 "weather-storm",                   "dark"],
    "221": ["lu apxa rawmpxom",                "weather-storm",                 "weather-storm",                   "dark"],
    "230": ["lu rawmpxom, ulte tompameyp zup", "weather-storm",                 "weather-storm",                   "dark"],
    "231": ["lu rawmpxom, ulte tompameyp zup", "weather-storm",                 "weather-storm",                   "dark"],
    "232": ["lu rawmpxom, ulte tompa zup",     "weather-storm",                 "weather-storm",                   "dark"],
    
    // drizzle
    "300": ["tompameyptsyìp zup",              "weather-showers-scattered-day", "weather-showers-scattered-night", "bright"],
    "301": ["tompameyp zup",                   "weather-showers-scattered-day", "weather-showers-scattered-night", "bright"],
    "302": ["tompameyp zup",                   "weather-showers-scattered-day", "weather-showers-scattered-night", "bright"],
    "310": ["tompameyp zup",                   "weather-showers-scattered-day", "weather-showers-scattered-night", "bright"],
    "311": ["tompameyp zup",                   "weather-showers-scattered-day", "weather-showers-scattered-night", "bright"],
    "312": ["tompa zup",                       "weather-showers-day",           "weather-showers-night",           "bright"],
    "313": ["tompa 'eko",                      "weather-showers",               "weather-showers",                 "dark"],
    "314": ["tompa 'eko",                      "weather-showers",               "weather-showers",                 "dark"],
    "321": ["tompameyp zup",                   "weather-showers-scattered-day", "weather-showers-scattered-night", "bright"],
    
    // rain
    "500": ["tompameyp zup",                   "weather-showers-scattered-day", "weather-showers-scattered-night", "bright"],
    "501": ["tompa zup",                       "weather-showers-day",           "weather-showers-night",           "bright"],
    "502": ["txana tompa zup",                 "weather-showers",               "weather-showers",                 "dark"],
    "503": ["tompa 'eko",                      "weather-showers",               "weather-showers",                 "dark"],
    "504": ["tompa 'eko 'eko nìtxan",          "weather-showers",               "weather-showers",                 "dark"],
    "511": ["tskxaytsyìp zup",                 "weather-hail",                  "weather-hail",                    "dark"],
    "520": ["tompameyp zup",                   "weather-showers",               "weather-showers",                 "dark"],
    "521": ["tompa zup",                       "weather-showers",               "weather-showers",                 "dark"],
    "522": ["txana tompa zup",                 "weather-showers",               "weather-showers",                 "dark"],
    "531": ["tompa zup",                       "weather-showers",               "weather-showers",                 "dark"],
    
    // snow
    "600": ["hermeyp zup",                     "weather-snow-scattered-day",    "weather-snow-scattered-night",    "bright"],
    "601": ["herwì zup",                       "weather-snow-scattered-day",    "weather-snow-scattered-night",    "bright"],
    "602": ["txana herwì zup",                 "weather-snow",                  "weather-snow",                    "dark"],
    "611": ["tomperwì zup",                    "weather-snow-rain",             "weather-snow-rain",               "dark"],
    "612": ["txana tomperwì zup",              "weather-snow",                  "weather-snow",                    "dark"],
    "615": ["tompameyp sì herwì zup",          "weather-snow",                  "weather-snow",                    "dark"],
    "616": ["tompa sì herwì zup",              "weather-snow",                  "weather-snow",                    "dark"],
    "620": ["txanfwerwì zup",                  "weather-snow",                  "weather-snow",                    "dark"],
    "621": ["txanfwerwì zup",                  "weather-snow",                  "weather-snow",                    "dark"],
    "622": ["txana txanfwerwì zup",            "weather-snow",                  "weather-snow",                    "dark"],
    
    // atmosphere
    "701": ["taw lu leyapay",                  "weather-mist",                  "weather-mist",                    "dark"],
    "711": ["tawìl nga' kxenerit",             "weather-mist",                  "weather-mist",                    "dark"],
    "721": ["taw lu leyapay",                  "weather-mist",                  "weather-mist",                    "dark"],
    "731": ["fkeytok fwopx",                   "weather-mist",                  "weather-mist",                    "dark"],
    "741": ["taw lu leyapay",                  "weather-mist",                  "weather-mist",                    "dark"],
    "751": ["tawìl nga' nenit",                "weather-mist",                  "weather-mist",                    "dark"],
    "761": ["fkeytok fwopx",                   "weather-mist",                  "weather-mist",                    "dark"],
    "762": ["tawìl nga' txepìvati txepramä",   "weather-mist",                  "weather-mist",                    "dark"],
    "771": ["hufwe lu ketsuksrese'a",          "weather-mist",                  "weather-mist",                    "dark"],
    "781": ["fkeytok nawma yrrap",             "weather-storm",                 "weather-storm",                   "dark"],
    
    // clouds
    "800": ["taw lu piak nìwotx",              "weather-clear",                 "weather-clear-night",             "bright"],
    "801": ["taw lu lepwopx nìhol",            "weather-few-clouds",            "weather-few-clouds-night",        "bright"],
    "802": ["taw lu lepwopx",                  "weather-clouds",                "weather-clouds-night",            "bright"],
    "803": ["taw lu lepwopx nìpxay",           "weather-many-clouds",           "weather-many-clouds",             "dark"],
    "804": ["taw lu tstu nìwotx",              "weather-many-clouds",           "weather-many-clouds",             "dark"],
    
    // extreme
    "900": ["fkeytok nawma yrrap",             "weather-windy",                 "weather-windy",                   "dark"],
    "901": ["fkeytok apxa yrrap",              "weather-windy",                 "weather-windy",                   "dark"],
    "902": ["fkeytok nawma yrrap",             "weather-windy",                 "weather-windy",                   "dark"],
    "903": ["ya lu txawew",                    "weather-windy",                 "weather-windy",                   "dark"],
    "904": ["ya lu txasom",                    "weather-windy",                 "weather-windy",                   "dark"],
    "905": ["hufwe tul nìwin",                 "weather-windy",                 "weather-windy",                   "dark"],
    "906": ["tskxaytsyìp zup",                 "weather-hail",                  "weather-hail",                    "dark"],
    
    // additional
    "951": ["hufwe ke tìran",                  "weather-windy",                 "weather-windy",                   "dark"],
    "952": ["hì'ia hufwetsyìp tìran",          "weather-windy",                 "weather-windy",                   "dark"],
    "953": ["lefpoma hufwetsyìp tìran",        "weather-windy",                 "weather-windy",                   "dark"],
    "954": ["hufwetsyìp tìran",                "weather-windy",                 "weather-windy",                   "dark"],
    "955": ["hufwe tìran",                     "weather-windy",                 "weather-windy",                   "dark"],
    "956": ["hufwe tìran nìwin",               "weather-windy",                 "weather-windy",                   "dark"],
    "957": ["hufwe tul",                       "weather-windy",                 "weather-windy",                   "dark"],
    "958": ["hufwe tul nìwin",                 "weather-windy",                 "weather-windy",                   "dark"],
    "959": ["hufwe tul nìwin nìngay",          "weather-windy",                 "weather-windy",                   "dark"],
    "960": ["fkeytok yrrap",                   "weather-windy",                 "weather-windy",                   "dark"],
    "961": ["fkeytok apxa yrrap",              "weather-windy",                 "weather-windy",                   "dark"],
    "962": ["fkeytok nawma yrrap",             "weather-windy",                 "weather-windy",                   "dark"],
}

function showWeather() {
    var url = 'http://api.openweathermap.org/data/2.5/weather?q=' +
                $('#city-field').val() +
                '&APPID=01285943f491282d208bd957bc6230ac&units=metric'
    $.getJSON(url, {}, function(data) {
        $('#place-field').html(data['name'] + ' (' + data['sys']['country'] + ')')
        
        var weatherText = weatherStrings[data['weather'][0]['id']]
        $('#general-field').html(weatherText[0])
        
        if (weatherText[3] === 'bright') {
            $('body').addClass('bright');
            $('body').removeClass('dark');
            $('body').removeClass('night');
        } else if (weatherText[3] === 'dark') {
            $('body').removeClass('bright');
            $('body').addClass('dark');
            $('body').removeClass('night');
        }
        
        $('#now-weather-image').css('background-image', 'url(ayrel/' + weatherText[1] + '.png)')
        
        $('#temperature-field').html('Ya lu ' +
                getTemperatureString(data['main']['temp']) +
                '. Nì\'Ìnglìsì, somwew lu ' + holpxay(Math.round(data['main']['temp'])) + ' &deg;C.')
        
        $('#clouds-field').html('Taw lu ' +
                getCloudsString(data['clouds']['all'])/* +
                ' (lu ayfìwopx mì ' +
                holpxay(Math.round(data['clouds']['all'] / 100.0 * 64.0)) +
                ' % tawä)'*/ +
                '.')
        
        $('#wind-field').html(getWindString(data['wind']['speed'])/* +
                ' (' + holpxay(Math.round(data['wind']['speed'])) + ' km/h, ftu ' +
                holpxay(Math.round(data['wind']['deg'])) + ' &deg;)'*/ +
                '.')
        
        $('#now-row').slideDown()
        $('#copyright-row').slideDown()
        $('#prediction-row').slideDown()
    })
}

function getTemperatureString(temperature) {
    if (temperature < -10) {
        return "txawew nìngay"
    } else if (temperature < 0) {
        return "txawew"
    } else if (temperature < 7) {
        return "wew"
    } else if (temperature < 12) {
        return "wur"
    } else if (temperature < 17) {
        return "tsyafe"
    } else if (temperature < 22) {
        return "sang"
    } else if (temperature < 27) {
        return "som"
    } else if (temperature < 35) {
        return "txasom"
    } else {
        return "txasom nìngay"
    }
}

function getCloudsString(clouds) {
    if (clouds < 5) {
        return "piak; ke lu kea pìwopx"
    } if (clouds < 10) {
        return "piak; lu hola pìwopx ahì'i nì'aw"
    } else if (clouds < 40) {
        return "lepwopx nìhol"
    } else if (clouds < 60) {
        return "lepwopx"
    } else if (clouds < 80) {
        return "lepwopx nìpxay"
    } else {
        return "tstu nìwotx"
    }
}

function getWindString(wind) {
    if (wind < 1) {
        return "Ke lu kea hufwe"
    } else if (wind < 5) {
        return "Stum ke lu kea hufwe"
    } else if (wind < 11) {
        return "Hufwetsyìp tìran"
    } else if (wind < 19) {
        return "Hufwe tìran nìk'ong"
    } else if (wind < 28) {
        return "Hufwe tìran"
    } else if (wind < 38) {
        return "Hufwe tìran nìtxan"
    } else if (wind < 49) {
        return "Hufwe tul nìk'ong"
    } else if (wind < 61) {
        return "Hufwe tul"
    } else if (wind < 74) {
        return "Hufwe tul nìwin"
    } else if (wind < 88) {
        return "Hufwe tul nìwin nìtxan"
    } else if (wind < 102) {
        return "Hufwe tul nìwin nìtxan nìngay"
    } else if (wind < 117) {
        return "Hufwe lu lehrrap nì'it"
    } else if (wind < 133) {
        return "Hufwe lehrrap tul"
    } else if (wind < 149) {
        return "Hufwe lehrrap 'eko"
    } else if (wind < 166) {
        return "Apxa hufwe lehrrap 'eko"
    } else if (wind < 183) {
        return "Apxa hufwe lehrrap 'eko nìtxan"
    } else if (wind < 202) {
        return "Hufwel ska'a stum fra'ut"
    } else if (wind < 252) {
        return "Hufwel ska'a fra'ut"
    } else if (wind < 1000) {
        return "Tìska'a nì'aw" // windspeed > 252 km per h for super hurricane, cyclons, typhoons...
    } else {
        return "Tätxaw ne 'Rrta"
    }
}

// Fìvefyatsyìpìl ralpeng holpxayit Sawtuteyä (vomun / 10), ne holpxay leNa'vi (vol / 8).
function holpxay(n) {
    return n.toString().replace(".", "").replace(",", "").replace(/(\d+)/g, function(n) {
        return parseInt(n, 10).toString(8) + "<sub>8</sub>"
    })
}
