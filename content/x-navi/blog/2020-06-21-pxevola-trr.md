---
id: 2020-06-21-pxevola-trr
title: Pxevola trr
kind: article
tags:
 - random
created_at: 2020-06-21 21:44
summary: "Txantsana fpeioä hum!"
needs_popups: true
---

{Mì <a href="https://kelutral.org">Helutral</a>, lamu hapxìturu alu Pamìrìk säfpìl atxantsan.\At the Kelutral, member Pamìrìk had a great idea.}
{Po pamrel soli tìpawmur apxevol, ulte mok futa pxevola trro, frapo pängkxo nìNa'vi nì'it teri sìpawm tsatrrä, ulte stä'nìpam si.\He wrote down 24 questions, and suggested that for 24 days, everyone chats a bit in Na'vi about that day's question, and records that.}
{Fìpostìfa oe starsìm oeyä aystä'nìpam.\In this post, I collect my recordings.}

{Tseri rutxe: pole'un oel futa new oe stä'nìpam si luke fwa srekrr hawl txankrr. Oe pängkxo nìyey.\Please take note: I decided that I want to record myself without preparing beforehand for a long time. I just chat.}
{Ha tsunslu fwa aystä'nìpamìl nga' kxeyeyt apxay.\So it is possible that my recordings contain a lot of errors.}

{Ma frapo a fìfpeioä leiu hapxìtu: siva ko!\To everyone who takes part in this challenge: good luck!}

<%= render "/audio.html", :url => "/static/audio/2020-06-21-pxevola-trr/1", :description => "2020-06-21 (trr a&deg;1ve): Pesu lu nga? Läpawk nì'it ko!" %>
<%= render "/audio.html", :url => "/static/audio/2020-06-21-pxevola-trr/2", :description => "2020-06-22 (trr a&deg;2ve): Pelun ftia fìlì'fyati?" %>
<%= render "/audio.html", :url => "/static/audio/2020-06-21-pxevola-trr/3", :description => "2020-06-23 (trr a&deg;3ve): Sla'tsu kemit a si fratrr." %>
<%= render "/audio.html", :url => "/static/audio/2020-06-21-pxevola-trr/4", :description => "2020-06-24 (trr a&deg;4ve): Paylì'u leNa'vi lu ngaru ngäzìk frato?" %>
<%= render "/audio.html", :url => "/static/audio/2020-06-21-pxevola-trr/5", :description => "2020-06-25 (trr a&deg;5ve): Txo ngal tivok Eywevengit 'awa kintrro, tsakrr nga nivew pehem sivi?" %>
<%= render "/audio.html", :url => "/static/audio/2020-06-21-pxevola-trr/6", :description => "2020-06-26 (trr a&deg;6ve): Sla'tsu ngeyä säsulìnit." %>
<%= render "/audio.html", :url => "/static/audio/2020-06-21-pxevola-trr/7", :description => "2020-06-27 (trr a&deg;7ve): Lì'fyayä leNa'vi tìftiari, slantire si nga peu?" %>

