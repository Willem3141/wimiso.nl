---
id: 2017-01-01-zisit-amip
title: "Zìsìt amip"
kind: article
created_at: 2017-01-01 21:40
tags:
 - navi-blog-month
summary: "'Upxare a fa mokri"
needs_popups: true
---

<%= render "/audio.html", :url => "/static/audio/2017-01-01-zisit-amip", :description => "'Upxare a fa mokri" %>

{Pamrel\text}
------

{Kaltxì ma frapo\hello everyone}! {Nì'awve\firstly}, {zola'u nìprrte' ne oeyä pìlok leNa'vi\welcome at my Na'vi blog}. {Na zìsìtam\like last year} {nìmun lu oe hapxìtu vospxìyä ayfìlokä leNa'vi\I again am a member of the month of the Na'vi blogs}, {ulte set ngal stawm postìt a'awve\and now you hear the first post}. {Srane\yes}, {plltxe oe san ‘stawm’ sìk\I said ‘hear’}, {taluna fìzìsìt oe fmi ngivop ayfostìti mokriyä\because this year I try to create voice posts}. {Lì'fyari leNa'vi\as for the Na'vi language}, {fpìl oel futa\I think that} {fwa pamrel si lu ftue to fwa tsun tslivam aylì'uti apawnlltxe\to write is easier than to understand spoken words}. {Ha\so}, {new oe fmivi ngivop ayfostìti fa mokri\I want to try creating posts by voice}. {Txo nga ke tsun tslivam\if you cannot understand}, {tsakrr sngum rä'ä si\then don't worry}: {pamrelit fìpostìyä sìsyung oel trray\I will add the text of this post tomorrow}. {Ke omum oel teyngta oe flìyä ngivop postìti frakintrr\I don't know if I will be able to create a post every week}, {slä\but}... {sìlpey oe tsafya\I hope so}.

{Tse, ma eylan\well, my friends}, {mipa zìsìt lefpom lìyevu\may the new year be peaceful}. {Zene oe pivlltxe\I have to say}: {oeri, zìsìtam ke lamu fe'\for me, last year was not bad}. {Krro krro txintìnìl oeti veykamrrìn\sometimes my work kept me busy}, {slä nìteng samunu oeru oeyä txintìn\but I also liked my work}.

{Zìsìtayìri new oe fayhem sivi\as for next year, I want to do those things}:

* {Nì'awve\firstly}, {mipa aylì'uri lì'fyayä leNa'vi nume\learn new Na'vi words}. ({Kezemplltxe\obviously}!)
* {Ulte nìmuve\and secondly}, {hahaw nì'ul\sleep more}, {taluna pxìm 'efu ngeyn\because I often feel tired}. (Ììì... {zene oe pivlltxe san ‘zìsìtam nìtengfya nolew’ sìk\I have to say, ‘last year I wanted the same’}. {Ke <s>flera</s> flerä\I did not succeed}.)

{Nìsyen\lastly}: {tse'a oel futa (nìmun) nga ke tsun sivung säplltxeviti mì oeyä pìlok\I see that (again) you cannot add comments in my blog}, {taluna lu fwel\because it's broken}. {Fmìsyi oe zeykivo tsat\I'll try to fix that}.

{Hayalovay\until the next time}!
