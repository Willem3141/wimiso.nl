---
id: 2019-12-31-ayrum-kxumpayä
title: "'Em ayrumit kxumpayä a fya'o"
kind: article
tags:
 - random
created_at: 2019-12-31 16:35
summary: "'Em ayrumit kxumpayä a fì'uri fìtrr oe srung sìmi sa'nokur. Txo nga new ivomum fya'ot a 'em tsat, tsakrr fìpostìti rutxe inan."
needs_popups: true
header_image: /static/img/2019-12-31-ayrum-kxumpayä/IMG_20191231_162823.jpg
---

{Fìtrr lu syena trr zìsìtä atalun oeyä sa'nokìl na frazìsìt 'olem aysyeyt a fko syaw nìNeytrrlan _oliebollen_.\Today is the last day of the year, so like every year my mother cooked snacks that are called in Dutch ‘oliebollen’.}
{Ral tsatstxoä lu _ayrum kxumpayä_, taluna fkol 'em tsat kxumpayfa.\The meaning of that name is ‘balls of oil’, because they are cooked in oil.}
{Fìfnetsyeytsyìp Neytrrlanmì stum fraporu smon ulte lok tì'i'a zìsìtä a krr pxaya tutel 'em tsat.\In the Netherlands almost everyone knows this type of snack and many people cook it when the end of the year comes near.}

{Tì'usemìri fìtrr oe srung soli, ulte kxawm aynga new ivomum fya'ot a 'em tsat.\I helped cooking today, and maybe you want to know the way to cook them.}
{Tafral oel slìsya'tsu tsat fa fìpostì.\Therefore I will describe that in this post.}
{Syolep moel relit a'a'aw ha txo ngal ke tslivam oeyä tìsla'tsut leNa'vi tsakrr nìyey tìng nari ayrelur, ulte ngal tslayam nìsìlpey.\We took a few pictures so if you don't understand my description in Na'vi then just look at the pictures, and hopefully you'll get it.}

{Kin ngal fì'ut (rumfpi amrrvol):\You need this (for 40 balls):}

* {payit\water} (4 dl fu 14 fl oz);
* {tsyoti\flour} (250 g fu 9 oz);
* {okupit akxum\thick milk} (nì'Ìnglìsi: _butter_) (100 g fu 3½ oz);
* {txo ngal new: aymautiti ahì'i\if you want: small fruits} (nì'Ìnglìsì: _currants_) (100 g fu 3½ oz);
* {loiti apukap\6 eggs}.

{Yem nemfa huru payit sì okupit akxum, ulte yem huruti sìn txep fte som sleykivu.\Put the water and the thick milk into a cooking pot, and place the pot in a fire to make it hot.}
{Tengkrr 'awstengyem, sung tsyoti, ulte ngop 'awa rumit apxa.\While you mix it, add the flour, and make one large ball.}

<%= render "/image.html", :url => "/static/img/2019-12-31-ayrum-kxumpayä/IMG_20191231_111002.jpg", :description => "Rum apxa" %>

{'Aku huruti txepftu ulte tsakrr sung ayloiti (nì'awve 'awa loiti nì'aw ulte 'awstengyem nìltsan, tsakrr sung loiti amuve, tsakrr loiti apxeyve saylahe).\Remove the pot from the fire and then add the eggs (first only one egg, and mix well, then add the second egg, then the third egg and so on).}
{Nìsyen, sung aymautiti ahì'ì, txo ngal nivew tsat (fpìl oel futa ftxìlor nì'ul slu tsakrr).\Finally, add the small fruits, if you want that (I think that it becomes more tasty then).}

<%= render "/image.html", :url => "/static/img/2019-12-31-ayrum-kxumpayä/IMG_20191231_114722.jpg", :description => "Fra'uti 'awstengyolem" %>

{Nìhay, som sleyku kxumpayit fa huru alahe.\Next, make oil hot using another pot.}
{Moel solar kxumpayit syulangä alu _sunflower_, slä skxakep lahea fnekxumpay tam nìteng.\We used oil from a flower called ‘sunflower’, but likely another type of oil is okay too.}
{Zumìri a'awstengyawnem set ngop hì'ia ayrumit.\Now make small balls from the mixed material.}
{Nìngay zene livu hì'i tsayrum, taluna som slu a krr, apxa nì'ul slu nìteng.\These balls really need to be small, because while they become hot, they also become larger.}

<%= render "/image.html", :url => "/static/img/2019-12-31-ayrum-kxumpayä/IMG_20191231_150218.jpg", :description => "Yerem hì'ia rumit nemfa kxumpay" %>

{Yem a'awa rumit nemfa kxumpay asom ulte pey nìyol\Place a few balls into the hot oil and wait a bit} (nì'Ìnglìsì: _7 minutes_).
{Tsakrr ayrumit 'aku kxumpayftu ulte sìn yomyo yem.\Then remove the balls from the oil and place them onto a plate.}
{Hasey!\Done!}

<%= render "/image.html", :url => "/static/img/2019-12-31-ayrum-kxumpayä/IMG_20191231_162823.jpg", :description => "Yom nìprrte'!" %>
