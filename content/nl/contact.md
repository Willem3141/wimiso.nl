---
id: contact
title: Contact
---

Wil je me bereiken, dan kun je de volgende methoden gebruiken.

E-mail
------

Mijn e-mailadres is **willem *(apenstaartje)* wimiso *(punt)* nl**.

Je kunt me versleutelde mails sturen; daarvoor kun je [GPG](https://www.gnupg.org/) gebruiken. Mijn openbare GPG-sleutel heeft de vingerafdruk <code>0EEDE9EC</code>; als het goed is kun je hem ophalen van een willekeurige keyserver. Als je zelf geen GPG gebruikt, kun je gebruik maken van [Encrypt.to](https://encrypt.to/0x0eede9ec).

XMPP
----

Je kunt me ook vinden op XMPP; mijn XMPP-account is **willem *(apenstaartje)* jabber *(punt)* no-sense *(punt)* net**.

