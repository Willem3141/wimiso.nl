---
id: thesis
title: Thesis
header_image: /static/img/thesis-header.png
---

My PhD thesis is entitled **Algorithms for River Network Analysis**. The full text can be downloaded [here](https://research.tue.nl/en/publications/algorithms-for-river-network-analysis).

The thesis contains many figures, which are almost all created with [Ipe](https://ipe.otfried.org) (for 2D figures) and [Blender](https://blender.org). On this page, you can download the Ipe and Blender source for a selection of figures.

Ipe figures are provided in PDF format. You'll need a recent Ipe version (for example 7.2.12) and the [thesis-figure-style.isy](/static/files/thesis/thesis-figure-style.isy) Ipe style file to properly open them. Related figures are generally grouped together in a single PDF file with several pages. For easy inclusion on websites and the like, SVG files (generated from the Ipe source by `iperender`) are provided as well.

Blender source files can be opened with Blender 2.79. To convert a Blender figure into an Ipe figure, I first rendered the figure normally into a raster image with Cycles, and then I rendered an SVG image containing the outlines with Freestyle. These two images are then combined into an Ipe figure. The process is shown in more detail [here](https://www.win.tue.nl/aga/tutorials/ipe/#using-blender-figures-in-ipe). Note that in many cases I extensively edited the resulting figure in Ipe.

<div class="alert alert-info">
    All figures on this page are licensed under the <a href="https://creativecommons.org/licenses/by/4.0/legalcode">Creative Commons Attribution 4.0</a> license, which in short means that anyone is free to re-use them for any purpose, provided that credit is given.
</div>

## Introduction

<div class="thesis-gallery">
    <%= render "/thesis/figure-tile.html", :number => "1.3", :file => "braided-river-schematized" %>
    <%= render "/thesis/figure-tile.html", :number => "1.4a / 1.6", :file => "smooth-terrain", :pages => 6, :blender => "smooth-terrain" %>
    <%= render "/thesis/figure-tile.html", :number => "1.4b / 1.9 / 1.10 / 3.17", :file => "discrete-terrain", :pages => 11, :blender => "discrete-terrain" %>
    <%= render "/thesis/figure-tile.html", :number => "1.5", :file => "critical-points", :pages => 4, :blender => "critical-points" %>
    <%= render "/thesis/figure-tile.html", :number => "1.7", :file => "not-transversal", :pages => 2, :blender => "not-transversal" %>
    <%= render "/thesis/figure-tile.html", :number => "1.8", :file => "star-link" %>
    <%= render "/thesis/figure-tile.html", :number => "1.11", :file => "split-tree-example", :blender => "split-tree-example" %>
    <%= render "/thesis/figure-tile.html", :number => "1.12", :file => "persistence-pairs" %>
    <%= render "/thesis/figure-tile.html", :number => "1.13", :file => "convex-hull-kds", :pages => 3 %>
</div>

## Chapter 2

<div class="thesis-gallery">
    <%= render "/thesis/figure-tile.html", :number => "2.1", :file => "algorithm-overview" %>
    <%= render "/thesis/figure-tile.html", :number => "2.3", :file => "non-saddle-rerouting" %>
    <%= render "/thesis/figure-tile.html", :number => "2.4", :file => "nphard-3-partition", :blender => "nphard-3-partition" %>
</div>

## Chapter 3

<div class="thesis-gallery">
    <%= render "/thesis/figure-tile.html", :number => "3.1", :file => "striation-entire-terrain-changes", :pages => 3 %>
    <%= render "/thesis/figure-tile.html", :number => "3.2", :file => "pruning-example", :pages => 2, :blender => "split-tree-example" %>
    <%= render "/thesis/figure-tile.html", :number => "3.3", :file => "cut-around-saddle", :blender => "split-tree-example" %>
    <%= render "/thesis/figure-tile.html", :number => "3.4 / 3.6 / 3.15", :file => "example-terrain", :pages => 6 %>
    <%= render "/thesis/figure-tile.html", :number => "3.5", :file => "event-list" %>
    <%= render "/thesis/figure-tile.html", :number => "3.7 / 3.8", :file => "area-above-ray", :pages => 3 %>
    <%= render "/thesis/figure-tile.html", :number => "3.9", :file => "stabbed-edge-finding" %>
    <%= render "/thesis/figure-tile.html", :number => "3.10", :file => "area-xt-plane" %>
    <%= render "/thesis/figure-tile.html", :number => "3.11", :file => "quadratic-counterexample", :pages => 2 %>
    <%= render "/thesis/figure-tile.html", :number => "3.12 / 3.16", :file => "handling-area-events", :pages => 2 %>
    <%= render "/thesis/figure-tile.html", :number => "3.13", :file => "handling-birth-events" %>
    <%= render "/thesis/figure-tile.html", :number => "3.14", :file => "deep-split-tree" %>
</div>

## Chapter 4

<div class="thesis-gallery">
    <%= render "/thesis/figure-tile.html", :number => "4.2", :file => "implementation-overview", :pages => 2 %>
    <%= render "/thesis/figure-tile.html", :number => "4.3", :file => "saddle-splitting" %>
    <%= render "/thesis/figure-tile.html", :number => "4.4", :file => "overlaying-networks" %>
    <%= render "/thesis/figure-tile.html", :number => "4.7", :file => "saddle-volume", :blender => "saddle-volume" %>
</div>

## Chapter 5

<div class="thesis-gallery">
    <%= render "/thesis/figure-tile.html", :number => "5.1a", :file => "base-surface-1", :blender => "base-surface" %>
    <%= render "/thesis/figure-tile.html", :number => "5.1b", :file => "base-surface-2", :blender => "base-surface" %>
    <%= render "/thesis/figure-tile.html", :number => "5.1c", :file => "base-surface-3", :blender => "base-surface" %>
    <%= render "/thesis/figure-tile.html", :number => "5.2a", :file => "surface-flat", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.2b", :file => "surface-hill", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.2c", :file => "surface-sloped-hill-2", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.3a", :file => "surface-valley", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.3b", :file => "surface-valley-base", :blender => "surfaces-2" %>
    <%= render "/thesis/figure-tile.html", :number => "5.3c", :file => "surface-valley-morph", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.5a", :file => "surface-sloped-hill", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.5b", :file => "surface-sloped-hill-rp", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.5c", :file => "surface-sloped-hill-wfs", :blender => "surfaces-2" %>
    <%= render "/thesis/figure-tile.html", :number => "5.6", :file => "surface-valley-2", :blender => "surfaces" %>
    <%= render "/thesis/figure-tile.html", :number => "5.7", :file => "triangle-volume", :blender => "triangle-volume" %>
</div>

## Chapter 6

<div class="thesis-gallery">
    <%= render "/thesis/figure-tile.html", :number => "6.2", :file => "linear-layout-example", :pages => 2 %>
    <%= render "/thesis/figure-tile.html", :number => "6.3", :file => "bin-and-strip-packing", :pages => 5 %>
    <%= render "/thesis/figure-tile.html", :number => "6.4", :file => "block-measures" %>
    <%= render "/thesis/figure-tile.html", :number => "6.5", :file => "rows-separate" %>
    <%= render "/thesis/figure-tile.html", :number => "6.6", :file => "np-hardness" %>
    <%= render "/thesis/figure-tile.html", :number => "6.7", :file => "packing-greedy-is-not-optimal" %>
    <%= render "/thesis/figure-tile.html", :number => "6.8", :file => "connector-routing" %>
    <%= render "/thesis/figure-tile.html", :number => "6.9", :file => "connector-routing-blocks" %>
    <%= render "/thesis/figure-tile.html", :number => "6.10", :file => "binary-search" %>
    <%= render "/thesis/figure-tile.html", :number => "6.13", :file => "process-tree-example" %>
    <%= render "/thesis/figure-tile.html", :number => "6.15", :file => "block-types" %>
</div>
