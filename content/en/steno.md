---
id: steno
title: Steno and Plover
toc: true
---

My typing skills are not so great. While I am able to type quite quickly in bursts, my technique is bad and I often make mistakes. A few months ago I became interested in stenography, which is a way of typing in which you type entire syllables or words in one go. This used to be a pretty obscure thing, used mostly by court reporters and live captioners, and costing lots and lots of money to get started with -- you'd need special software, a special keyboard, and so on. Luckily a few years ago this changed because a group of volunteers started the Open Steno Project, which made a Free Software stenography program called Plover.

To learn stenography and Plover, a large number of resources and tutorials are available, most notably [Learn Plover](https://sites.google.com/site/ploverdoc/home). So why did I still wrote the one that you are now reading? Actually, mostly as a way to help myself learn. I put it online because maybe it is useful for others as well, but please make sure to check the other materials as well.

## Background

The idea behind stenography (or simply steno) is that the thing taking most time when typing on a conventional keyboard is not the number of key presses, it's the fact that you have to press them all in the correct order. If you could just press all of the keys in a word at once, then you would be able to type much faster. (Just think of how many typos happen because you accidentally swap two keys around.) Of course on a normal keyboard that doesn't work, because how would you keep anagrams apart -- and how would you even be able to hit so many keys at once?

Therefore steno uses a special keyboard. It contains 23 keys, which are positioned such that you can hit almost any key combination easily. To hit any combination, you don't even need to move your fingers much. In fact, each finger is responsible for two neighboring keys on the keyboard. In the resting position, it sits right between the two keys. To press a finger's both keys, you press down in that position, and to press just one of the keys, you move the finger slightly before pressing. That is basically all the movement needed to type.

Let's take a look at the steno keyboard in detail.

<center>
	<img src="/static/img/steno/steno-keyboard.svg">
</center>

The keyboard is divided into three parts. On the left, there are some consonants. You hit those keys with your left hand. On the right, there are some more consonants (note that some are the same as the ones on the left -- we'll see why in a minute). You hit those with your right hand. And finally, on the bottom, there are four vowel keys. The vowel keys are hit by the thumbs of both hands. Here's a picture of which keys belong to which finger.

<center>
	<img src="/static/img/steno/steno-fingers.svg">
</center>

(As you can see, I lied a bit before: some fingers take care of more or less than two keys... this will be explained when we get to those keys.)

So what is the reason that we need multiple copies of some of the keys, while some letters are not represented on the keyboard at all? That has to do with the most important rule of steno:

> The keys pressed in a stroke should (generally) be read from left to right on the steno keyboard. That is, first the consonants on the left `STKPWHR`, then the vowels `AOEU`, then the consonants on the right `FRPBLGTSDZ`.

<center>
	<img src="/static/img/steno/steno-order.svg">
</center>

This is called steno order, and ensures that the keystrokes are decipherable. For example, this distinguishes the words *tar* (`TAR`) and *rat* (`RAT`). And this explains why we need more than one copy of the same key, namely to be able to write words like *pop* (`POP`).

<center>
	<img src="/static/img/steno/tar-rat-pop.svg">
</center>

But of course there is more going on here. Namely, some letters are outright missing on the keyboard, and there are definitely not enough copies of all of the keys to type longer words. It turns out that you can write the missing letters by hitting combinations of other keys. We'll see in the next sections how that works exactly.


## Vowels

Let's start our discussion of the keys by looking at the four vowel keys:

<center>
	<img src="/static/img/steno/vowels.svg">
</center>

As mentioned before, you press `AO` with the thumb of your left hand. To press `A` or `O` separately, move your thumb sideways slightly before pressing. Similarly, you press `EU` with the thumb of your right hand.


### Short vowels

Pressing one vowel key produces a short vowel. The keys `A`, `O`, `E` and `U` do what you'd expect them to do: they produce the short English vowels *a*, *o*, *e* and *u*. But how do we write the short *i* sound? Well, that's where our first key combination comes into play: it is stroked as `EU`. It's not like there's anything special about `EU` -- it just so happens that English doesn't have an *eu* sound, so we can nicely (ab)use that key combination for the *i* sound.

<table>
<tr>
	<td><img src="/static/img/steno/vowels-a.svg"></td>
	<td><code>A</code></td>
	<td>
		<b>short <i>a</i></b><br>
		as in <i>hat</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-o.svg"></td>
	<td><code>O</code></td>
	<td>
		<b>short <i>o</i></b><br>
		as in <i>hot</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-e.svg"></td>
	<td><code>E</code></td>
	<td>
		<b>short <i>e</i></b><br>
		as in <i>met</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-u.svg"></td>
	<td><code>U</code></td>
	<td>
		<b>short <i>u</i></b><br>
		as in <i>hut</i>, <i>tough</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-i.svg"></td>
	<td><code>EU</code> = <code>I</code></td>
	<td>
		<b>short <i>i</i></b><br>
		as in <i>hit</i>, <i>women</i>
	</td>
</tr>
</table>

 This ‘`EU`-for-*i*’-thing is so common that after a short while it gets second nature. From now on we'll write `EU` as `I` in stroke descriptions, because this is so much easier to read.


### Long vowels

To write a long vowel, you press the corresponding short vowel, together with the two keys on the other side. As an exception, the long *o* sound is stroked as `OE` (instead of `OI`) because `OI` is already used for the *oi* sound (we'll see that below).

<table>
<tr>
	<td><img src="/static/img/steno/vowels-ai.svg"></td>
	<td><code>AI</code></td>
	<td>
		<b>long <i>a</i></b><br>
		as in <i>hate</i>, <i>hay</i>, <i>hey</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-oe.svg"></td>
	<td><code>OE</code></td>
	<td>
		<b>long <i>o</i></b><br>
		as in <i>moat</i>, <i>mode</i>, <i>bowl</i>, <i>foe</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-aoe.svg"></td>
	<td><code>AOE</code></td>
	<td>
		<b>long <i>e</i></b><br>
		as in <i>eat</i>, <i>meet</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-aou.svg"></td>
	<td><code>AOU</code></td>
	<td>
		<b>long <i>u</i></b><br>
		as in <i>mute</i>, <i>hue</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-aoi.svg"></td>
	<td><code>AOI</code></td>
	<td>
		<b>long <i>i</i></b><br>
		as in <i>hide</i>, <i>hi</i>, <i>high</i>, <i>height</i>, <i>guy</i>, <i>fly</i>, <i>bye</i>, <i>eye</i>, <i>lie</i>, <i>type</i>
	</td>
</tr>
</table>

Unfortunately, English spelling is a complete mess in general, and with regard to long vowels in particular. For example, the words *heat* and *meet* both have a long *e* vowel, but they're spelled in two different ways. Now a very important principle of steno comes into play:

> Steno is (mostly) phonetic, in other words, you type a word like it is pronounced -- not like it is written. (There are exceptions, but we'll get to that later.)

So whether you want to type *hide*, *lie*, *type*, you type all those vowels as `AOI`. This is done for two reasons. Firstly, there are simply not enough keys available for all of those weird spellings. Secondly, when typing at speed, you don't really want to think about how a word is spelled. Instead, you can just type a word like you hear it.


### Vowel disambiguation

There's one big disadvantage to this system of typing phonetically. Namely, English has a lot of words that are spelled differently, but pronounced the same. Therefore, the basic steno rules don't distinguish between them. Think about the words *I* and *eye*, for example, or *eat* and *meet*. Of course such words still need their own strokes (Plover doesn't try to guess for you, since that would be unreliable). To disambiguate sometimes another vowel gets substituted, a so-called disambiguator.

<table>
<tr>
	<td><img src="/static/img/steno/vowels-ae.svg"></td>
	<td><code>AE</code></td>
	<td>
		<b>disambiguator for long <i>e</i></b><br>
		among two conflicting words, the one spelled with <i>ea</i> gets this one<br>
		example: <i>flee</i> (with <code>AOE</code>) / <i>flea</i> (with <code>AE</code>)
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-ao.svg"></td>
	<td><code>AO</code></td>
	<td>
		<b>disambiguator for long <i>u</i></b><br>
		among two conflicting words, the one spelled with <i>oo</i> gets this one<br>
		example: <i>route</i> (with <code>AOU</code>) / <i>root</i> (with <code>AO</code>)
	</td>
</tr>
</table>

There are several other ways to disambiguate, which will be explained later.


### Diphtongs

English has several diphtongs, which get their own key combinations.

<table>
<tr>
	<td><img src="/static/img/steno/vowels-au.svg"></td>
	<td><code>AU</code></td>
	<td>
		<b>diphtong <i>aw</i></b><br>
		as in <i>caught</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-ou.svg"></td>
	<td><code>OU</code></td>
	<td>
		<b>diphtong <i>ow</i></b><br>
		as in <i>how</i>, <i>plough</i>
	</td>
</tr>
<tr>
	<td><img src="/static/img/steno/vowels-oi.svg"></td>
	<td><code>OI</code></td>
	<td>
		<b>diphtong <i>oi</i></b><br>
		as in <i>alloy</i>
	</td>
</tr>
</table>


### Other uses of long vowels

`MOR` (*more*) -> `MOER` (*mother*)


## Initial consonants


## Final constants


## Briefs

### One-key briefs


## Longer words


### Affixes


### Multi-syllable words


## Other strokes

Besides the strokes that we have seen until now, which produce words or syllables, there are also strokes that do different things. There are strokes for single letters, strokes for punctuation, and strokes that change the way Plover handles spaces and capitalization.


### Fingerspelling

For writing names that are not in the dictionary, Plover provides a set of strokes that do nothing else than producing single letters. (This is also useful when you don't know the stroke for a word, of course.) This way, you can at least spell out the word. This is called fingerspelling.

Note: The word fingerspelling comes from sign language, which has a set of signs to express single letters. Hence, spelling with your fingers.

The fingerspelling strokes are quite easy to learn, because the fingerspelling stroke for a letter is simply the initial-consonant or vowel stroke for that sound, together with the `*` key. For the letters that are not represented on the keyboard as an initial consonant or vowel, there are some special strokes:

<table>
<tr>
	<td><code>KR*</code></td>
	<td><b>c (fingerspelling)</b></td>
</tr>
<tr>
	<td><code>KW*</code></td>
	<td><b>q (fingerspelling)</b></td>
</tr>
<tr>
	<td><code>KP*</code></td>
	<td><b>x (fingerspelling)</b></td>
</tr>
<tr>
	<td><code>SG*</code></td>
	<td><b>z (fingerspelling)</b></td>
</tr>
</table>

The fingerspelling strokes are implemented by *glue strokes*. That means that between fingerspelled letters, no spaces will be inserted, but around a fingerspelled word, spaces will be added as normal. Therefore you can easily insert a fingerspelled word in a sentence. Fingerspelled letters will not be capitalized automatically (this allows you to type words like *iPod*). To capitalize a fingerspelled letter, you can add the `-P` key to the stroke.

To speed up the rather slow process of fingerspelling, there are a few strokes for frequent digraphs:

<table>
<tr>
	<td><code>KH*</code></td>
	<td><b>ch (fingerspelling)</b></td>
</tr>
<tr>
	<td><code>GL*</code></td>
	<td><b>gl (fingerspelling)</b></td>
</tr>
<tr>
	<td><code>SH*</code></td>
	<td><b>sh (fingerspelling)</b></td>
</tr>
<tr>
	<td><code>TH*</code></td>
	<td><b>th (fingerspelling)</b></td>
</tr>
</table>

You can also use all of the affix strokes. Just like when using them with normal words, they will be automatically attached to the fingerspelled word.

If you're using a fingerspelled word too often, you should add it to the dictionary, so that you can just type it in one stroke. We'll see how that is done at the end of this chapter.


### Punctuation and control strokes

Of course to type punctuation, you shouldn't need to fall back on your Qwerty keyboard. Here are the four most important punctuation marks.

<table>
<tr>
	<td><code>TP-PL</code></td>
	<td><b>. (period)</b></td>
</tr>
<tr>
	<td><code>KW-BG</code></td>
	<td><b>, (comma)</b></td>
</tr>
<tr>
	<td><code>TP-BG</code></td>
	<td><b>! (exclamation mark)</b></td>
</tr>
<tr>
	<td><code>KW-PL</code></td>
	<td><b>? (question mark)</b></td>
</tr>
</table>

<div class="alert alert-info">
	There are actually several ways to stroke the punctuation marks. What I'm showing here is just the way I type them.
</div>

 Those strokes may seem a bit random at first, but if you try to type them, you'll see that they were chosen for their shape. Plover will by the way automatically handle capitalization of the first word of the next sentence, and will also put a space behind them. If you need to override that for some reason (maybe you're pregramming and you need to type `System.out` without the space), you can use the following strokes.

<table>
<tr>
	<td><code>KPA</code></td>
	<td><b>capitalize</b><br>capitalizes the next word and puts a space before it</td>
</tr>
<tr>
	<td><code>KPA*</code></td>
	<td><b>capitalize (no space)</b><br>capitalizes the next word and doesn't put a space before it</td>
</tr>
<tr>
	<td><code>D-LS</code></td>
	<td><b>delete space</b><br>lowercases the next word and doesn't put a space before it</td>
</tr>
<tr>
	<td><code>S-P</code></td>
	<td><b>space</b><br>forces a space to be inserted</td>
</tr>
</table>

These strokes are also very useful if you are switching from one program to another. Plover doesn't know what program you're typing into, so if you switch to another program or input field within a program, it doesn't know whether it should capitalize and/or insert a space or not. I got used to pressing one of these control keys whenever I switch to another input field.


### Defining new strokes

Another really useful control stroke is the stroke that is used to add a word to the dictionary.

<table>
<tr>
	<td><code>DUPT</code></td>
	<td><b>dictionary update</b><br>opens a window to add a word to the dictionary</td>
</tr>
</table>

You use this whenever you need to type a word that is not in the dictionary. To see how this works, let's say you wanted to type *QuickSort*. This is not in the dictionary, so you have to type it by fingerspelling (or you use `KPA*` with the strokes for *quick* and *sort*). After you entered the word, enter the `DUPT` stroke. Then type the stroke you would like to assign to this word -- for example `KWIK/SORT`. It'll show you if there are any conflicts. If you are happy with the stroke, you can save with `R-R`. And now you can type *QuickSort* using your newly assigned stroke. The important thing is that you can do this without even taking your fingers off the steno keyboard, so you can do it quickly if the need arises.

(More of those...)

