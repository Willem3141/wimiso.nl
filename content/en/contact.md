---
id: contact
title: Contact
---

If you want to contact me, you can use the following methods.

Email
-----

My email address is **willem *(at)* wimiso *(dot)* nl**.

I welcome encrypted mail; you can use [GPG](https://www.gnupg.org/). My public GPG key has fingerprint <code>0EEDE9EC</code>; you should be able to retrieve it from any keyserver. Otherwise, if you don't use GPG yourself, you can use [Encrypt.to](https://encrypt.to/0x0eede9ec).

XMPP
----

You can also find me on XMPP; my XMPP account is **willem *(at)* jabber *(dot)* no-sense *(dot)* net**.

