---
id: 2019-10-11-powerline-fish-duration
title: "Command durations in Powerline"
kind: article
created_at: 2019-10-11 11:52
needs_highlighting: true
tags:
 - tricks
---

[Powerline](https://github.com/powerline/powerline) is an easy way to make your shell prompt more informative (and fancy). One of the things it shows is the exit code (if non-zero) of the last command. It seems also useful to show the duration of the last command.

<!-- more -->

The [fish](https://fishshell.com/) shell conveniently provides an environment variable `CMD_DURATION` that stores the duration of the last command in milliseconds, and someone on GitHub [showed how to display that in Powerline](https://github.com/powerline/powerline/issues/1416). I wanted to make this a little nicer by displaying the duration only if the command actually took, say, more than half a second to run, and also by converting the time to seconds.

The following fish function does just that:

    #!fish
    function fish_postexec --on-event fish_postexec
        if test $CMD_DURATION -gt 500
            set --export --global CMD_DURATION_STR (math --scale=1 $CMD_DURATION / 1000)s
        else
            set --erase CMD_DURATION_STR
        end
        # avoid the duration being shown more than once
        # (this can happen when pressing Enter without entering a command)
        set CMD_DURATION 0
    end

To use it, put this segment in your Powerline theme:

    #!json
    {
        "function": "powerline.segments.common.env.environment",
        "priority": 10,
        "args": {
            "variable": "CMD_DURATION_STR"
        }
    }

<%= render "/image.html", :url => "/static/img/2019-10-11-powerline-fish-duration.png", :description => "The result" %>
