---
id: 2019-10-10-links-awakening
title: "Japanese vocabulary in Link's Awakening"
kind: article
created_at: 2019-10-10 23:41
draft: true
tags:
 - japanese
---

One of the more fun ways to practice a foreign language, in my opinion, is playing games in that language. Because I'm trying to learn Japanese, and I am a fan of Zelda games, I decided to play Link's Awakening (or <ruby>夢<rt>ゆめ</rt></ruby>を<ruby>見<rt>み</rt></ruby>る<ruby>島<rt>しま</rt></ruby>) fully in Japanese.

<!-- more -->

Although my Japanese is not very good, this turns out to be surprisingly doable. Firstly, you don't need to understand every single word to understand the gist of what is said, and secondly, online dictionaries make it really easy to look up words you don't know. Still, it would have been more fun if I wouldn't have had to look up words all the time. Therefore I searched for a list of vocabulary used in the game, but couldn't find any. Now that I finished the game, this blog post is my attempt at making such a list.

I included all words that I didn't know or wasn't sure about, and that I couldn't guess from context. I'm somewhere between JLPT N4 and N5, so "words that I didn't know" includes many words.

If you want to use the list, I think it is best to learn these words before you start the game. Alternatively you can keep the list around while playing (the words are given roughly in the order they appear in the game).

**Note about spoilers:** I tried to avoid any spoilers, but of course the inclusion of certain words in the list can give some small things away.

## Weapons and items

Let's start with some generic Zelda terms that appear throughout the game.

| 剣 | けん / つるぎ | sword |
| 縦 | たて | shield |
| 妖精 | ようせい | fairy |
| 秘密 | ひみつ | secret |
| 魔物 | まもの | monster |
| 粉 | こな | powder |
| 器 | うつわ | container |
| 手がかり | てがかり | clue |
| 宝箱 | たからばこ | treasure chest |
| カギ | | key |

## Introduction

| みたい | | look like |
| 酷く | ひどく | very much |
| 魘される | うなされる | to be restless while having a nightmare |
| 意識 | いしき | consciousness |
| 意識する | いしきする | to be conscious |
| はっきり |  | clearly |
| 沿う | そう | to run along |
| 流れ来る | ながれくる | to be washed ashore |
| 浜辺 | はまべ | beach |
| 怪物 | かいぶつ | monster |
| ウロウロ |  | (onomatopoeia for aimless movement) |
| ウロウロする |  | to walk around aimlessly |
| 不思議 | ふしぎ | incredible |
| 裏 | うら | below, backside |
| ほら！ |  | look at that! |
| 戻る | もどる | to return |
| 戻ってくる | もどってくる | to come back |
| 押す | おす | to push, to press (a button) |
| 弾く | はじく | to repel |
| 敵 | てき | enemy |
| 台座 | だいざ | pedestal |
| タンス |  | chest of drawers |
| 勝手に | かってに | as you please, just |
| 覚える | おぼえる | to remember, to learn |
| 打ち上げる | うちあげる | to wash ashore |
| 探し | さがし | in search of |

## Village

| 発生 | はっせい | outbreak, pest |
| 釣り堀 | つりぼり | fish pond |
| ニワトリ |  | chicken |
| びくともしない |  | staying unperturbed |
| 画面 | がめん | screen |
| 開く | ひらく | to open |
| さっぱり |  | nothing at all (with negative verb) |
| おまかせ |  | leave it to ... |
| 迷う | まよう | to lose your way |
| 草原 | そうげん | grassland |
| ずっと |  | all the way, much (more) |
| 再開 | さいかい | restart |
| やり直す | やりなおす | to retry |
| 欲しがる | ほしがる | to desire |
| 取る | とる | to take, to pick up |
| 取れる | とれる | to be obtainable |
| いずれ |  | anyway |
| どうやら |  | seemingly |
| 間違い | まちがい | mistake |
| 間違える | まちがえる | to make a mistake |
| オシャレ |  | fashion |
| 毛並み | けなみ | breeding (of an animal) |
| 自慢 | じまん | pride |
| 印 | しるし | mark, sign, pin |
| 洞穴 | ほらあな | cave |
| 欠片 | かけら | fragment |
| 確認 | かくにん | check |

## Beach

| 坊や | ぼうや | boy |
| 主 | あるじ | master |
| 魔物 | まもの | demon |
| 告げる | つげる | to announce |
| 使者 | ししゃ | messenger |
| 現れる | あらわれる | to appear, to become visible |
| 無くす | なくす | to lose (something) |
| 振り回す | ふりまわす | to wield (a sword) |
| 管理人 | かんりにん | manager |
| 珍しい | めずらしい | unusual |
| 缶詰 | かんずめ | canned food |
| 趣味 | しゅみ | hobby |
| 芸術家 | げいじゅつか | artist |
| 素手 | すで | bare hand |
| 触る | さわる | to touch |

## Forest

| 決して | けっして | definitely not, not at all |
| 理 | ことわり | reason |
| 成り立つ | なりたつ | to be valid (of a reasoning), to make sense |
| 治める | おさめる | to govern |
| 限り | かぎり | limit, boundary |
| 疲れた | つかれた | tired, worn-out |
| 訪ねる | たずねる | to visit |
| 傷 | きず | wound |
| 癒す | いやす | to heal |
| 湧く | わく | to well up, to awaken |
| 〜っぽい |  | -ish, -like |

## First dungeons

| 増える | ふえる | to increase |
| 沼 | ぬま | swamp |
| 花咲く | はなさく | to blossom |
| それこそ |  | unmistakably, especially |
| やはり |  | as expected, also |
| 度重なる | たびかさする | to be frequent |
| 無礼 | ぶれい | impoliteness |
| 許す | ゆるす | to forgive |
| 〜べき |  | should, ought to (particle for things that should be done) |
| 示す | しめす | to tell, to demonstrate |
| 確かめる | たしかめる | to check, to ascertain |
| 攫う | さらう | to kidnap |
| 取り替える | とりかえる | to exchange |
| 昼寝 | ひるね | nap, siesta |
| 狙う | ねらう | to have an eye on, to aim for |
| 救出 | きゅうしゅつ | rescue |
| 物騒 | ぶっそう | dangerous |
| 連れる | つれる | to take (something) with you |
| 寄る | よる | to approach, to drop by |
| 逃げ出す | にげだす | to run away |
| 置く | おく | to put |
| 黄金 | おうごん | gold |
| 取り返す | とりかえす | to get back, to regain |
| わけじゃない |  | I don't mean ... |
| サル |  | monkey |
| 出来上がり | できあがり | completion |
| さらば |  | farewell |
| 残す | のこそ | to leave behind |
| 棒きれ | ぼうきれ | stick |
| 怪しい | あやしい | suspicious |
| ラクチン |  | easy-peasy |
| 像 | ぞう | statue |
| 穴蔵 | あなぐら | cellar |
| 得体 | えたい | nature |
| 塊 | かたまり | lump |
| 突っ込む | つっこむ | to thrust into (something) |
| 壊す | こわす | to break |
| 敵 | かたき | rival |
| 殻 | から | shell |
| 内側 | うちがわ | inside |
| 攻撃 | こうげき | attack |
| 加える | くわえる | to add, to inflict |
| 滝 | たき | waterfall |
| いくつ |  | how many |
| 奏でる | かなでる | to play an instrument |
| 砂漠 | さばく | desert |
| 砂 | すな | sand |
| 手がかり | てがかり | clue |
| 幸せ | しあわせ | happiness |
| 蜂蜜 | はちみつ | honey |
| 蜂の巣 | はちのす | honeycomb |
| 姉妹提携都市 | しまいていけいとし | sister cities |
| 海象 | せいうち | walrus |
| ほこら |  | small shrine |
| 秘める | ひめる | to hide |
| 並ぶ | ならぶ | to line up, to equal |
| 墓 | はか | gravestone |
| 宿る | やどる | to live in |

## Marin

| 実 | み | seed |
| 向こう | むこう | opposite side |
| きっと |  | surely |
| カモメ |  | gull |
| 祈る | いのる | to pray |
| 叶う | かなう | to come true |
| かしら |  | ... I wonder |
| いつか |  | one day, in due course |
| 借る | かる | to borrow |
| 寝起き | ねおき | waking up |
| 飛び降りる | とびおりる | to jump down |
| 吹く | ふく | to blow, to play a flute |

## More dungeons

| 壺 | つぼ | basin of a waterfall |
| お化け | おばけ | ghost |
| 懐かしい | なつかしい | nostalgic |
| 連れる | つれる | to take (something with you) |
| ご無沙汰 | ごぶさた | not visiting for a long time |
| 入り江 | いりえ | bay |
| マンボウ |  | ocean sunfish |
| ナマズ |  | catfish |
| 壺 | つぼ | jar |
| 囲む | かこむ | to surround |
| 衝撃 | しょうげき | shock |
| 与える | あたえる | to give |
| 珊瑚 | さんご | coral |
| 恐れる | おそれる | to fear |
| 現実 | げんじつ | reality |
| 支配 | しはい | rule, domination |
| 邪魔 | じゃま | nuisance |
| 塔 | とう | tower |
| ごとく |  | like, the same as (adverb) |
| 羽ばたく | はばたく | to flap your wings |
| 担ぐ | かつぐ | to carry on your shoulder |
| 風見鶏 | かぜみどり | wind vane |
| 解ける | とける | to be solved |
| 蘇る | よみがえる | to be resurrected |
| 喜ぶ | よろこぶ | to accept with pleasure |
| 両翼 | りょうよく | both flanks (of a mountain) |
| 大鷲 | おおわし | sea eagle

## Additional tips

In the game there are certain statues that speak in katakana, which makes it hard to figure out what is meant. What's more, they speak in some archaic dialect of Japanese (it took me half an hour before I figured that out), where in particular い-adjectives end in き instead. So タカキ should be read as たかい = 高い.
