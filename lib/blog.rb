def sorted_articles_in_language(lang)
  sorted_articles.select do |i|
    i[:language] == lang && !i[:draft]
  end
end

def sorted_articles_in_language_tag(lang, tag)
  items_with_tag(tag).select do |i|
    i[:language] == lang && !i[:draft]
  end
end

def get_post_excerpt(post)
  if post[:summary] then
    post[:summary]
  else
    content = post.compiled_content
    if content =~ /\s<!-- more -->\s/
      content = content.partition('<!-- more -->').first
    end
    content
  end
end

def get_post_start(post)
  get_post_excerpt(post) +
    "<div class='read-more'><a href='#{post.path}'>#{_(:read_more)} ›</a></div>"
end

def build_feed(lang, title, author_name, author_uri)
  output = "<rss version=\"2.0\">"
  output += "<channel>"
  output += "<title>" + title + "</title>\n"

  for post in sorted_articles_in_language(lang) do
    output += "<item>"
    output += "<title>" + post[:title] + "</title>"
    output += "<description>" + get_post_excerpt(post) + "</description>"
    output += "<link>https://wimiso.nl" + post.path.to_s + "</link>"
    output += "<pubDate>" + Time.parse(post[:created_at]).rfc2822 + "</pubDate>"
    output += "</item>\n"
  end

  output += "</channel>"
  output += "</rss>"
  output
end
